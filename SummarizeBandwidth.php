#!/usr/bin/php
<?php

class SummarizeBandwidth
{
  const EXPECTED_COLS = 6;

  const FIELD_TIMESTAMP  = 0;
  const FIELD_SERVERCODE = 1;
  const FIELD_PING       = 2;
  const FIELD_DOWNSPEED  = 3;
  const FIELD_UPSPEED    = 4;
  // top

  const KEY_DOWN = 'down';
  const KEY_UP   = 'up';
  const KEY_PING = 'ping';


  static protected $SERVERS = array(
    '4309',
    '3128',
    '3165',
    '1776',
    '1355'
  );

  protected $filename;
  protected $fileContents;
  protected $parsedContent = array();
  protected $summaryContent = array();

  public function __construct ($filename)
  {
    if (is_file($filename)) {
      if (is_readable($filename)) {
        $this->filename = $filename;
      } else {
        trigger_error('Unreadable file: ' . $filename . '.');
        exit;
      }
    } else {
      trigger_error('Invalid file: ' . $filename . '.');
      exit;
    }

    $this->parse();
    $this->generateSummary();
    $this->outputCSV();
  }

  public function getResult ()
  {
    return $this->summaryContent;
  }

  protected function parse ()
  {
    $this->fileContents = file($this->filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    if ($this->fileContents === false) {
      trigger_error('Unable to read file.');
      exit;
    }

    $stamp = 0;
    $serverCode = self::$SERVERS[0];
    $lastServerCode = end(self::$SERVERS);

    $dtzObj = new DateTimeZone('America/Kentucky/Louisville');

    // First, make a run through to see if the lines are as expected.
    $lines = count($this->fileContents);
    for ($i=1; $i<$lines; $i++) {
      $csvLine = str_getcsv($this->fileContents[$i]);
      $temp = count($csvLine);

      /*
       * For a valid line, add the values to $parsedContent.
       */
      if ($temp === self::EXPECTED_COLS) {
        // round time to nearest hour
        $stamp = new DateTime($csvLine[self::FIELD_TIMESTAMP], $dtzObj);
        $stamp = $stamp->format('YmdH');

        $serverCode = $csvLine[self::FIELD_SERVERCODE];
        $this->parsedContent[$stamp][$serverCode] = array(
          self::KEY_DOWN => $csvLine[self::FIELD_DOWNSPEED],
          self::KEY_UP   => $csvLine[self::FIELD_UPSPEED],
          self::KEY_PING => $csvLine[self::FIELD_PING],
        );
      }

      /*
       * If the field count is off for this line, erase any
       * in-progress $stamp and skip to the next $SERVERS.
       */
      else {
        echo 'Line ', ($i+1), ': Invalid field count (', $temp, ").\n";

// 				/*
// 				 * See if entry for latest $stamp has expected server count.
// 				 */
// 				if (count($this->parsedContent[$stamp]) == count(self::$SERVERS)) {
// 					// Completed last set and borked the beginning of the next.
// 					do {
// 						$lookaheadLine = str_getcsv($this->fileContents[$i++]);
// 						echo 'Skipped line ', ($i+1), ".\n";
// // 						echo 'count($lookaheadLine): ', count($lookaheadLine), "\n";
// // 						echo '$lookaheadLine[self::FIELD_SERVERCODE]: ', $lookaheadLine[self::FIELD_SERVERCODE], "\n";
// // 						echo '$this->fileContents[$i]: ', $this->fileContents[$i], "\n";
// // 						echo 'strpos($this->fileContents[$i], "self::$SERVERS[0]"): ', strpos($this->fileContents[$i], '"' . self::$SERVERS[0] . '"'), "\n";
// // 						exit;
// 					} while (
// 						   count($lookaheadLine) != self::EXPECTED_COLS
// 						or $lookaheadLine[self::FIELD_SERVERCODE] != self::$SERVERS[0]
// // 						or strpos($this->fileContents[$i], '"' . self::$SERVERS[0] . '"') === false
// 						or $i >= $lines
// 				    );
// 					/*
// 					 * After completing do/while, decrement $i (since we
// 					 * looked ahead) and skip to next iteration.
// 					 */
// 					$i--;
// 					echo 'Backing up to line ', ($i+1), ".\n";
// 					continue;
// 				}
      }
    } // end for-loop
  }

  protected function generateSummary ()
  {
    $serverCount = count(self::$SERVERS);
    foreach ($this->parsedContent as $stamp => $entries) {
      $count = count($entries);
      if ($count !== $serverCount) {
        echo 'Server count mismatch (', $count, ') for ', $stamp, ".\n";
        continue;
      }

      $avgDown = $avgUp = $avgPing = 0;

      foreach ($entries as $data) {
        $avgDown += $data[self::KEY_DOWN];
        $avgUp   += $data[self::KEY_UP];
        $avgPing += $data[self::KEY_PING];
      }
// 			$avgDown = $avgDown/$serverCount;
// 			$avgUp   = $avgUp/$serverCount;
// 			$avgPing = $avgPing/$serverCount;
      $avgDown = $avgDown/$count;
      $avgUp   = $avgUp/$count;
      $avgPing = $avgPing/$count;

      $this->summaryContent[$stamp] = array(
        'down' => $avgDown,
        'up'   => $avgUp,
        'ping' => $avgPing,
      );
    }
  }

  protected function outputCSV ()
  {
    $outfile = dirname($this->filename) . '/bandwidth-out.csv';
// 		if (file_exists($outfile)) {
// 			trigger_error('Output file exists: ' . $outfile . '.');
// 			exit;
// 		}

    $fh = fopen($outfile, 'w');

    // header row
    fputcsv($fh, array(
      'date',
      'down',
      'up',
      'ping'
    ));

    foreach ($this->summaryContent as $stamp => $data) {
      $fullStamp = substr($stamp, 0, 4) . '-' . substr($stamp, 4, 2) . '-' . substr($stamp, 6, 2) . ' ' . substr($stamp, 8, 2) . ':00';
      array_unshift($data, $fullStamp);
      fputcsv($fh, $data);
    }
  }
}

$bw = new SummarizeBandwidth('~/Dropbox/bandwidth.csv');
