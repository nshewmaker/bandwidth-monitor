#!/usr/bin/env bash
set -e

SPEEDTEST_CLI='~/Scripts/speedtest-cli'

# Initialize file.
logFile=$HOME/Dropbox/bandwidth.csv
if [[ ! -e $logFile || ! -s $logFile ]]
then
  # If we're creating the file for the first time, add column headers.
  echo '"timestamp","server","ping (ms)","down (Mb/s)","up (Mb/s)","running"' > $logFile
fi

# First, look at running processes. This can help
# explain abnormalities in bandwidth. For example,
# a browser or a download client may be very busy.
#
# We monitor for 3 iterations (-l 3) and take only
# the last, because in non-event mode, the first is
# always 0. After that, top tends to use the highest
# percentage of CPU as it initializes.
proc=$(top -o cpu -stats command,cpu -n 5 -l 3 | tail -n 5)

# Pick a few Speedtest.net locations. Retrieve the
# list using speedtest-cli --list, which orders them
# by increasing distance.
for server in 935 5827
do
  ( echo -n '"'; date '+%Y-%m-%d %H:%M:%S' | tr -d '\n'; echo -n '",';) | tee -a $logFile
  ( echo -n "\"$server\","; ) | tee -a $logFile
  result=$($SPEEDTEST_CLI --server $server --simple);
  # Mac OS uses -Ee. Linux uses -re.
  ( echo -n $result | sed -Ee 's/Ping: ([0-9]+\.[0-9]+) ms Download: ([0-9]+\.[0-9]+) Mbit\/s Upload: ([0-9]+\.[0-9]+) Mbit\/s/"\1","\2","\3",/' | tr -d '\n'; ) | tee -a $logFile
  ( echo    '"'${proc% }'"'; ) | tee -a $logFile
done
